<?php
/**
 * Customizer sections.
 *
 * @package Sudoseo
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function sudoseo_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'sudoseo_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'sudoseo' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'sudoseo_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'sudoseo' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'sudoseo' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'sudoseo_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'sudoseo' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'sudoseo_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'sudoseo' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'sudoseo_customize_sections' );
