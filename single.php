<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Sudoseo
 */

get_header();
$thumb = get_the_post_thumbnail_url();

?>
	<div class=" content-area">
		<main id="main" class="site-main">
		<div class="single-header grid-x" style="background-image: url('<?php echo esc_url( $thumb ); ?>')">
			<header class="entry-header">
			<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
			if ( 'post' === get_post_type() ) :
			?>
			<?php endif; ?>
			</header><!-- .entry-header -->
		</div>

			<div class=" grid-x single-cont">
				<div class="primary  col-l-8">
					<div class="post-content">
						<div class="tiles">
							<?php
							while ( have_posts() ) :
								gt_set_post_view();
								the_post();

								get_template_part( 'template-parts/content', get_post_format() );

								the_post_navigation();
								?>
						</div>
						<div class=" tiles commentsbox">
								<?php

								// If comments are open or we have at least one comment, load up the comment template.
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;

							endwhile; // End of the loop.
							?>
						</div>
					</div>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</main><!-- #main -->
	</div>
<?php get_footer(); ?>
