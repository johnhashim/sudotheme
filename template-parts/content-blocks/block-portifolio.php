<?php
/**
 *  The template used for displaying fifty/fifty text/text.
 *
 * @package Sudoseo
 */

// Set up fields.

$portifolio = joh_get_posts_by_type( 'portifolio', 3 );
$animation_class = sudoseo_get_animation_class();

// Start a <container> with a possible media background.
sudoseo_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container portifolio wrap ', // The container class.
) );
?>
<div class="port grid-x<?php echo esc_attr( $animation_class ); ?>">



<?php
foreach ( $portifolio as $post ) :
	setup_postdata( $post );
	get_template_part( 'template-parts/content', 'portifolio' );//or the name of your tamplate you want to show 
	endforeach;
	wp_reset_postdata();
?>


</div> 
</section> 




<div class="blog-slider__item swiper-slide">
							<div class="blog-slider__img">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
							<?php the_post_thumbnail(); ?>
						</a>
							</div>
							<div class="blog-slider__content">
							<span class="blog-slider__code"><?php sudoseo_posted_on(); ?></span>
							<div class="blog-slider__title"><?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?></div>
							<div class="blog-slider__text"><?php the_excerpt(); ?></div>
							<a href="<?php the_permalink(); ?>" class="blog-slider__button">READ MORE</a>
							</div>
						</div>
