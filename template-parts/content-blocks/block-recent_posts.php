<?php
/**
 * The template used for displaying a recent posts block.
 *
 * This block will either display all recent posts or posts
 * from a specific category. The amount of posts can be
 * limited through the admin.
 *
 * @package Sudoseo
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$post_count      = get_sub_field( 'number_of_posts' );
$categories      = get_sub_field( 'categories' );
$tags            = get_sub_field( 'tags' );
$animation_class = sudoseo_get_animation_class();


function alexa( $domain, $format = false ) {
$uri  = 'https://data.alexa.com/data?cli=10&dat=snbamz&url=';
$uri .= $domain;

$xml = simplexml_load_file( $uri );

if ( isset( $xml->SD[1]->POPULARITY ) ) {
		$data = (int) $xml->SD[1]->POPULARITY->attributes()->TEXT;
}
	if ( $format ) {
return number_format( $data );
	}
	return $data;

}

function setAndViewPostViews( $postID ) {
	$count_key = 'views';
	$count     = get_post_meta( $postID, $count_key, true );
	if ( $count == '' ) {
		$count = 0;
		delete_post_meta( $postID, $count_key );
		add_post_meta( $postID, $count_key, '0' );
	} else {
		$count++;
		update_post_meta( $postID, $count_key, $count );
	}
	return $count; /* so you can show it */
}

// Variable to hold query args.
$args = array();

// Only if there are either categories or tags.
if ( $categories || $tags ) {
	$args = sudoseo_get_recent_posts_query_arguments( $categories, $tags );
}

// Always merge in the number of posts.
$args['posts_per_page'] = is_numeric( $post_count ) ? $post_count : 3;

// Get the recent posts.
$recent_posts = sudoseo_get_recent_posts( $args );

// Display section if we have any posts.
if ( $recent_posts->have_posts() ) :

	// Start a <container> with possible block options.
	sudoseo_display_block_options(
		array(
			'container' => 'section', // Any HTML5 container: section, div, etc...
			'class'     => 'content-block grid-container recent-posts wrap ', // Container class.
		)
	);
	?>

	<div class=" grid-x joshim <?php echo esc_attr( $animation_class ); ?>">
		<?php if ( $title ) : ?>
		<h2 class="content-block-title"><?php echo esc_html( $title ); ?></h2>
		<?php endif; ?>

<!-- Based on: https://dribbble.com/shots/4883028-Profile-Stats-->
<div class="layout">
	<div class="profile">
		<?php if ( get_sub_field( 'avatar' ) ) : ?>
		 <div class="profile__picture"><img src="<?php the_sub_field( 'avatar' ); ?>" alt="john Hashim" /></div>
		 <?php else : ?>
		 <div class="profile__picture"><img src="https://secure.gravatar.com/avatar/8140e9d8aab227779f9119bc3c9d137b?s=64" alt="john Hashim" /></div>
		<?php endif; ?>
		<div class="profile__header">
			<div class="profile__account">
				<h4 class="profile__username">John Hashim</h4>
			</div>
			<div class="profile__edit"><a class="profile__button" data-toggle="modal" data-target="#myModal" href="#">Message</a></div>
		</div>
		<div class="profile__stats">
			<div class="profile__stat">
				<div class="profile__icon profile__icon--gold">
				<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="gripfire" class="svg-inline--fa fa-gripfire fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 384 512">
					<path fill="currentColor" d="M112.5 301.4c0-73.8 105.1-122.5 105.1-203 0-47.1-34-88-39.1-90.4.4 3.3.6 6.7.6 10C179.1 110.1 32 171.9 32 286.6c0 49.8 32.2 79.2 66.5 108.3 65.1 46.7 78.1 71.4 78.1 86.6 0 10.1-4.8 17-4.8 22.3 13.1-16.7 17.4-31.9 17.5-46.4 0-29.6-21.7-56.3-44.2-86.5-16-22.3-32.6-42.6-32.6-69.5zm205.3-39c-12.1-66.8-78-124.4-94.7-130.9l4 7.2c2.4 5.1 3.4 10.9 3.4 17.1 0 44.7-54.2 111.2-56.6 116.7-2.2 5.1-3.2 10.5-3.2 15.8 0 20.1 15.2 42.1 17.9 42.1 2.4 0 56.6-55.4 58.1-87.7 6.4 11.7 9.1 22.6 9.1 33.4 0 41.2-41.8 96.9-41.8 96.9 0 11.6 31.9 53.2 35.5 53.2 1 0 2.2-1.4 3.2-2.4 37.9-39.3 67.3-85 67.3-136.8 0-8-.7-16.2-2.2-24.6z"></path>
				</svg>
				</div>
				<div class="profile__value">
					<?php echo setAndViewPostViews( get_the_ID() ); ?>
					<div class="profile__key">Page views</div>
				</div>
			</div>
			<div class="profile__stat">
				<div class="profile__icon profile__icon--blue">
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="signal" class="svg-inline--fa fa-signal fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 640 512">
					<path fill="currentColor" d="M216 288h-48c-8.84 0-16 7.16-16 16v192c0 8.84 7.16 16 16 16h48c8.84 0 16-7.16 16-16V304c0-8.84-7.16-16-16-16zM88 384H40c-8.84 0-16 7.16-16 16v96c0 8.84 7.16 16 16 16h48c8.84 0 16-7.16 16-16v-96c0-8.84-7.16-16-16-16zm256-192h-48c-8.84 0-16 7.16-16 16v288c0 8.84 7.16 16 16 16h48c8.84 0 16-7.16 16-16V208c0-8.84-7.16-16-16-16zm128-96h-48c-8.84 0-16 7.16-16 16v384c0 8.84 7.16 16 16 16h48c8.84 0 16-7.16 16-16V112c0-8.84-7.16-16-16-16zM600 0h-48c-8.84 0-16 7.16-16 16v480c0 8.84 7.16 16 16 16h48c8.84 0 16-7.16 16-16V16c0-8.84-7.16-16-16-16z"></path>
				</svg>
				</div>
				<div class="profile__value"><?php echo alexa( 'https://www.johnhashim.com', true ); ?>
					<div class="profile__key">Alexa Ranking</div>
				</div>
			</div>
			<div class="profile__stat">
				<div class="profile__icon profile__icon--pink">
				<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="grin-hearts" class="svg-inline--fa fa-grin-hearts fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 496 512">
					<path fill="currentColor" d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zM90.4 183.6c6.7-17.6 26.7-26.7 44.9-21.9l7.1 1.9 2-7.1c5-18.1 22.8-30.9 41.5-27.9 21.4 3.4 34.4 24.2 28.8 44.5L195.3 243c-1.2 4.5-5.9 7.2-10.5 6l-70.2-18.2c-20.4-5.4-31.9-27-24.2-47.2zM248 432c-60.6 0-134.5-38.3-143.8-93.3-2-11.8 9.2-21.5 20.7-17.9C155.1 330.5 200 336 248 336s92.9-5.5 123.1-15.2c11.4-3.6 22.6 6.1 20.7 17.9-9.3 55-83.2 93.3-143.8 93.3zm133.4-201.3l-70.2 18.2c-4.5 1.2-9.2-1.5-10.5-6L281.3 173c-5.6-20.3 7.4-41.1 28.8-44.5 18.6-3 36.4 9.8 41.5 27.9l2 7.1 7.1-1.9c18.2-4.7 38.2 4.3 44.9 21.9 7.7 20.3-3.8 41.9-24.2 47.2z"></path>
				</svg>
				</div>
				<div class="profile__value">
				<?php
					$count_posts     = wp_count_posts();
					$published_posts = $count_posts->publish;
					echo $published_posts;
				 ?>
					<div class="profile__key">Posts</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
	
	  <!-- Modal content-->
	  <div class="modal-content">
	  <?php
		$form = get_sub_field( 'contact_form' );
		gravity_form( $form, false, true, false, '', true, 1 );
		?>
		 
		<div class="modal-footer">
		  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	  </div>
	  
	</div>
  </div>

<!-- new cards -->



<div class="jh">
<div class="view">
	<ul class="card__list">
	<?php
		// Loop through recent posts.
		$count = 0;
		while ( $recent_posts->have_posts() ) :
		$count++;
		$recent_posts->the_post();
		?>
		<a class="fancy" href="<?php the_permalink(); ?>" rel="bookmark">
		<li class="card__item card__item">
			<div class="card__info">
				<div class="info-player">
					<p class="info-player__num" style="background-image:url('<?php the_post_thumbnail_url( 'thumb' ); ?>');"></p>
					<p class="info-player__name"><small><?php echo get_the_category()[0]->name; ?></small><br><?php the_title(); ?></p>
				</div>
				<div class="info-place">
		<sup>
		<?php
		$post_date = get_the_date( 'd M' );
		echo $post_date;
		?>
					</sup>
				</div>
			</div>
		</li>
		</a>

	<?php
			endwhile;
			wp_reset_postdata();
		endif;
		?>

	</ul>
</div>
</div>


	</div><!-- .grid-x -->
</section><!-- .recent-posts -->

