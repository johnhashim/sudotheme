<?php
/**
 * The template used for displaying related posts.
 *
 * @package Sudoseo
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$related_posts   = get_sub_field( 'related_posts' );
$animation_class = sudoseo_get_animation_class();

// Display section if we have any posts.
if ( $related_posts ) :

	// Start a <container> with possible block options.
	sudoseo_display_block_options(
		array(
			'container' => 'section', // Any HTML5 container: section, div, etc...
			'class'     => 'content-block grid-container related-posts', // Container class.
		)
	);

	?>

	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">
	<?php if ( $title ) : ?>
		<h2 class="content-block-title"><?php echo esc_html( $title ); ?></h2>
	<?php endif; ?>
	<div class="blog-slider">
		<div class="blog-slider__wrp swiper-wrapper">

		<?php
		// Loop through recent posts.
		foreach ( $related_posts as $key => $post ) :

			// Convert post object to post data.
			setup_postdata( $post );
			?>
			<div class="blog-slider__item swiper-slide">
				<div class="blog-slider__img">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				<?php the_post_thumbnail(); ?>
			</a>
				</div>
				<div class="blog-slider__content">
				<span class="blog-slider__code"><?php sudoseo_posted_on(); ?></span>
				<div class="blog-slider__title"><?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?></div>
				<div class="blog-slider__text"><?php the_excerpt(); ?></div>
				<a href="<?php the_permalink(); ?>" class="blog-slider__button">READ MORE</a>
				</div>
			</div>
<?php
		endforeach;
		wp_reset_postdata();
	?>
	</div>
	<div class="blog-slider__pagination"></div>
</div>
	</div><!-- .grid-x -->
</section><!-- .recent-posts -->
<?php endif; ?>
