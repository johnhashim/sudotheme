<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sudoseo
 */

?>
<article class="tile" tabindex="0">
		<div class="tile__img">
			<div class="img-wrapper">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'mobile-thumb' ); ?></a>
			</div>
		</div>
		<div class="tile__content">
			<div class="tile__metas">
				<span class="meta"><?php sudoseo_posted_on(); ?></span>
			</div>
			<h3 class="tile__title"><?php the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a>' ); ?></h3>
			<div class="tile__body">
			<?php the_excerpt(); ?>
			</div>
			<a href="<?php the_permalink(); ?>" class="tile__btn">Read Article!</a>
		</div>
</article><!-- #post-## -->
